package com.example.boundserviceexample;

import com.example.boundserviceexample.BoundService.MyBinder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	BoundService mBoundService;
	boolean mServiceBound = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final TextView timestampText = (TextView) findViewById(R.id.timestamp_text);
		Button printTimestampButton = (Button) findViewById(R.id.print_timestamp);
		Button stopServiceButon = (Button) findViewById(R.id.stop_service);
		printTimestampButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mServiceBound) {
					timestampText.setText(mBoundService.getTimestamp());
				}
			}
		});

		stopServiceButon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mServiceBound) {
					unbindService(mServiceConnection);
					mServiceBound = false;
				}
				Intent intent = new Intent(MainActivity.this,
						BoundService.class);
				stopService(intent);
			}
		});

	}

	@Override
	protected void onStart() {
		super.onStart();
		// Bind to boundService
		Intent intent = new Intent(this, BoundService.class);
		
		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		 // Unbind from the service
		if (mServiceBound) {
			unbindService(mServiceConnection);
			mServiceBound = false;
		}
	}
	 /** Defines callbacks for service binding, passed to bindService() */
	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.v("BoundService", "onServiceDisconnected");
			mServiceBound = false;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.v("BoundService", "on service connected" );
			 // We've bound to boundservice, cast the IBinder and get boundservice instance
			MyBinder myBinder = (MyBinder) service;
			mBoundService = myBinder.getService();
			mServiceBound = true;
		}

	};
}
